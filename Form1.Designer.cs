﻿namespace DNS_Flusher
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.ManualFlushButton = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.dnsServersText = new System.Windows.Forms.TextBox();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Log = new System.Windows.Forms.TextBox();
            this.CloseButton = new System.Windows.Forms.Button();
            this.MinimiseButton = new System.Windows.Forms.Button();
            this.TitlePanel = new System.Windows.Forms.Panel();
            this.Title = new System.Windows.Forms.Label();
            this.SysStartButton = new System.Windows.Forms.Button();
            this.contextMenuStrip1.SuspendLayout();
            this.TitlePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ManualFlushButton
            // 
            this.ManualFlushButton.Location = new System.Drawing.Point(12, 37);
            this.ManualFlushButton.Name = "ManualFlushButton";
            this.ManualFlushButton.Size = new System.Drawing.Size(127, 33);
            this.ManualFlushButton.TabIndex = 0;
            this.ManualFlushButton.Text = "Manual Flush";
            this.ManualFlushButton.UseVisualStyleBackColor = true;
            this.ManualFlushButton.Click += new System.EventHandler(this.ManualFlushButton_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // dnsServersText
            // 
            this.dnsServersText.BackColor = System.Drawing.Color.Gainsboro;
            this.dnsServersText.Location = new System.Drawing.Point(12, 76);
            this.dnsServersText.Multiline = true;
            this.dnsServersText.Name = "dnsServersText";
            this.dnsServersText.ReadOnly = true;
            this.dnsServersText.Size = new System.Drawing.Size(281, 189);
            this.dnsServersText.TabIndex = 1;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "DNS Flusher";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(104, 48);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.viewToolStripMenuItem.Text = "View";
            this.viewToolStripMenuItem.Click += new System.EventHandler(this.viewToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.closeToolStripMenuItem.Text = "Close";
            // 
            // Log
            // 
            this.Log.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Log.Location = new System.Drawing.Point(12, 271);
            this.Log.Name = "Log";
            this.Log.ReadOnly = true;
            this.Log.Size = new System.Drawing.Size(281, 22);
            this.Log.TabIndex = 2;
            // 
            // CloseButton
            // 
            this.CloseButton.BackColor = System.Drawing.SystemColors.ControlText;
            this.CloseButton.FlatAppearance.BorderSize = 0;
            this.CloseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CloseButton.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold);
            this.CloseButton.Location = new System.Drawing.Point(272, 0);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(33, 31);
            this.CloseButton.TabIndex = 3;
            this.CloseButton.Text = "x";
            this.CloseButton.UseVisualStyleBackColor = false;
            // 
            // MinimiseButton
            // 
            this.MinimiseButton.BackColor = System.Drawing.SystemColors.ControlText;
            this.MinimiseButton.FlatAppearance.BorderSize = 0;
            this.MinimiseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinimiseButton.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold);
            this.MinimiseButton.Location = new System.Drawing.Point(233, 0);
            this.MinimiseButton.Name = "MinimiseButton";
            this.MinimiseButton.Size = new System.Drawing.Size(33, 31);
            this.MinimiseButton.TabIndex = 4;
            this.MinimiseButton.Text = "-";
            this.MinimiseButton.UseVisualStyleBackColor = false;
            // 
            // TitlePanel
            // 
            this.TitlePanel.BackColor = System.Drawing.SystemColors.ControlText;
            this.TitlePanel.Controls.Add(this.Title);
            this.TitlePanel.Controls.Add(this.CloseButton);
            this.TitlePanel.Controls.Add(this.MinimiseButton);
            this.TitlePanel.ForeColor = System.Drawing.SystemColors.Control;
            this.TitlePanel.Location = new System.Drawing.Point(0, 0);
            this.TitlePanel.Name = "TitlePanel";
            this.TitlePanel.Size = new System.Drawing.Size(305, 31);
            this.TitlePanel.TabIndex = 5;
            // 
            // Title
            // 
            this.Title.BackColor = System.Drawing.SystemColors.ControlText;
            this.Title.ForeColor = System.Drawing.SystemColors.Control;
            this.Title.Location = new System.Drawing.Point(16, 0);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(207, 31);
            this.Title.TabIndex = 0;
            this.Title.Text = "DNS Flusher - 05:00:00 Until next flush.";
            this.Title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SysStartButton
            // 
            this.SysStartButton.Font = new System.Drawing.Font("Segoe UI Semibold", 8F, System.Drawing.FontStyle.Bold);
            this.SysStartButton.Location = new System.Drawing.Point(145, 37);
            this.SysStartButton.Name = "SysStartButton";
            this.SysStartButton.Size = new System.Drawing.Size(148, 33);
            this.SysStartButton.TabIndex = 6;
            this.SysStartButton.Text = "Run on System Start [OFF]";
            this.SysStartButton.UseVisualStyleBackColor = true;
            this.SysStartButton.Click += new System.EventHandler(this.SysStartButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(305, 305);
            this.Controls.Add(this.SysStartButton);
            this.Controls.Add(this.TitlePanel);
            this.Controls.Add(this.Log);
            this.Controls.Add(this.dnsServersText);
            this.Controls.Add(this.ManualFlushButton);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(305, 305);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(305, 31);
            this.Name = "Form1";
            this.Opacity = 0D;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DNS Flusher - 00:00:00 Until next flush.";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RefreshDNSAddressList);
            this.contextMenuStrip1.ResumeLayout(false);
            this.TitlePanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ManualFlushButton;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox dnsServersText;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.TextBox Log;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Button MinimiseButton;
        private System.Windows.Forms.Panel TitlePanel;
        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Button SysStartButton;
    }
}

