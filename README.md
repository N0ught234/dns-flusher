# README #

This README will document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* DNS cache is a store of data related to websites visited that was designed to speed up website processing time. Sometimes this can mess up so it can help to flush the DNS list.
* Version 0.0.1.0

### How do I get set up? ###

* Download the latest version from http://tomnaughton.net/projects/dnsflusher/
* There are no dependencies required to use DNS Flusher.
* after downloading the zip file, extract it using an archiver to a folder and cut the folder to where ever you want (try Program Files) and create a shortcut of the EXE file inside.

### Who do I talk to? ###

* Having a problem installing the application or you have a problem related to DNS Flusher? Contact me @ contact.tomn@gmail.com