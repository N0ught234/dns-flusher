﻿using Microsoft.Win32;
using System;
using System.Drawing;
using System.Media;
using System.Net;
using System.Windows.Forms;
using System.IO;
using System.Deployment.Application;
using System.Reflection;

namespace DNS_Flusher
{
    public partial class Form1 : Form
    {



        #region "Variables"

        #region "Public Variables"

        // Controls the countdown.
        public static int Time = 18000000;

        // Returns the version of the program. Used to compare to a file on a server to determine if there's an update available.
        public string ProgramVersion
        {
            get
            {
                if (ApplicationDeployment.IsNetworkDeployed)
                {
                    Version ver = ApplicationDeployment.CurrentDeployment.CurrentVersion;
                    return string.Format("{0}.{1}.{2}.{3}", ver.Major, ver.Minor, ver.Build, ver.Revision, Assembly.GetEntryAssembly().GetName().Name);
                }
                else
                {
                    var ver = Assembly.GetExecutingAssembly().GetName().Version;
                    return string.Format("{0}.{1}.{2}.{3}", ver.Major, ver.Minor, ver.Build, ver.Revision, Assembly.GetEntryAssembly().GetName().Name);
                }
            }
        }

        #endregion

        #region "Private Variables"
        // Size of the form at the start. Used to resize the form when the title bar is clicked (not dragged).
        private Size StartSize;
        private bool RunOnStartup;
        // The location of the temporary file of the version checker.
        private string VersionFile { get { return Path.GetTempPath() + "\\dnsflusherversionchecker_candelete.dnsf"; } }
        #endregion

        #endregion






        // Run on initialization.
        public Form1()
        {
            DNS.FlushDNS();



            #region "Check for an update"
            try
            {
                using (var InternetConnectionCheckWC = new WebClient())
                {
                    using (InternetConnectionCheckWC.OpenRead("http://clients3.google.com/generate_204"))
                    {

                        using (WebClient client = new WebClient())
                        {
                            client.DownloadFile("http://tomnaughton.net/projects/dnsflusher/versioncheck.dnsf",
                                                VersionFile);
                        }

                        string[] lines = File.ReadAllLines(VersionFile);
                        string version = lines[0];
                        if (version != ProgramVersion)
                        {
                            Console.WriteLine(ProgramVersion + " | " + version);
                            DialogResult dr = MessageBox.Show("[DNS Flusher] NEW version available! Download now?", "Update Available", MessageBoxButtons.YesNo);

                            if (dr == DialogResult.Yes)
                            {
                                System.Diagnostics.Process.Start(lines[1]);
                                Environment.Exit(0);
                            }
                        }

                    }
                }
            }
            catch
            {

            }
            
            #endregion


            // The 'Form' part of the code begins here.
            InitializeComponent();
            Log.Text = "Component initialized.";

            StartSize = Size;

            Log.Text = DNS.DisplayDnsAddresses(dnsServersText);

            RegistryKey rk = Registry.CurrentUser.OpenSubKey
                ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            string rosText = (string)rk.GetValue("DNS Flusher");
            if (rosText == Application.ExecutablePath)
            {
                RunOnStartup = true;
                SysStartButton.Text = "Run on System Start [ON]";
            }
            else
            {
                SysStartButton.Text = "Run on System Start [OFF]";
            }

            #region "Event handler setup"
            MinimiseButton.Click += HideForm;
            closeToolStripMenuItem.Click += CloseApplication;
            CloseButton.Click += CloseApplication;

            TitlePanel.MouseMove += TitleBar_MouseMove;
            TitlePanel.MouseDown += TitleBar_MouseDown;
            TitlePanel.MouseUp += TitleBar_MouseUp;

            Title.MouseMove += TitleBar_MouseMove;
            Title.MouseDown += TitleBar_MouseDown;
            Title.MouseUp += TitleBar_MouseUp;

            dnsServersText.KeyUp += RefreshDNSAddressList;
            TitlePanel.KeyUp += RefreshDNSAddressList;
            Title.KeyUp += RefreshDNSAddressList;
            KeyUp += RefreshDNSAddressList;
            #endregion

            timer1.Start();
        }










        // Toggles the from from full size to nothing but the title bar which displays the time left until the next flush.
        private void ToggleFormSize(object sender, EventArgs e)
        {
            Console.WriteLine("Form size toggled");
            if (Size == StartSize)
            {
                Size = Size.Empty;
            }
            else
            {
                Size = StartSize;
            }
        }

        #region "Custom title bar control"
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private bool MousePressed = false;
        private Point LastLocation;

        private void TitleBar_MouseDown(object sender, EventArgs e)
        {
            LastLocation = Location;
            MousePressed = true;
        }

        private void TitleBar_MouseMove(object sender, EventArgs e)
        {
            if (MousePressed)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void TitleBar_MouseUp(object sender, EventArgs e)
        {
            MousePressed = false;

            if (LastLocation == Location)
                ToggleFormSize(sender, e);
        }
        #endregion







        // Hides the form to the system tray.
        private void HideForm(object sender, EventArgs e)
        {
            Console.WriteLine("Hiding application...");
            notifyIcon1.Visible = true;
            Hide();
            Console.WriteLine("Application hidden.");
        }

        // Hides the form to the system tray when the user attempts to close it using Window's close button.
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Console.WriteLine("User attempted to close the form.");
            HideForm(sender, e);
            e.Cancel = true;
        }





        private void CloseApplication(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }





        // A DNS flush is performed when the user presses the manual flush button.
        private void ManualFlushButton_Click(object sender, EventArgs e)
        {
            Log.Text = "Manually flushing DNS...";
            Log.Text = DNS.FlushDNS();
            SystemSounds.Beep.Play();
            Log.Text = "Manual DNS flush was successful.";
        }

        // Code run every tick of timer1. The time is displayed in the form's title.
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Time > 0)
                Time -= 1000;
            else
                Log.Text = DNS.FlushDNS();

            if (Visible)
                Title.Text = "DNS Flusher - " + TimeSpan.FromMilliseconds(Time) + " Until next flush.";
        }







        #region "System Tray"

        // Displays the form to the user when the icon is double-clicked.
        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            Console.WriteLine("Notify icon double clicked.");
            Show();
        }

        // Displays the form to the user when the user clicks the toolstrip item.
        private void viewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Console.WriteLine("ToolStripItem - Show clicked.");
            Show();
        }

        #endregion




        // refreshes the DNS server list the keyboard key 'R' is pressed.
        private void RefreshDNSAddressList(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.R)
                Log.Text = DNS.DisplayDnsAddresses(dnsServersText);
        }




        // Adds or removed the program from the startup registry list.
        private void ToggleStartupState()
        {
            RegistryKey rk = Registry.CurrentUser.OpenSubKey
                ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            if (!RunOnStartup)
            {
                try
                {
                    rk.SetValue("DNS Flusher", Application.ExecutablePath);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occured. Run as administrator. If the error continues, please report the error. Error message: " + ex.Message);
                    return;
                }

                SysStartButton.Text = "Run on System Start [ON]";
            }
            else
            {
                try
                {
                    rk.DeleteValue("DNS Flusher", false);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occured. Run as administrator. If the error continues, please report the error. Error message: " + ex.Message);
                    return;
                }

                SysStartButton.Text = "Run on System Start [OFF]";
            }

            RunOnStartup = !RunOnStartup;
        }

        // toggles the system start state.
        private void SysStartButton_Click(object sender, EventArgs e)
        {
            ToggleStartupState();
        }

        // Run on initial startup. Since I want the form to start in tray, I need to set the opacity to 0, hide it and THEN set the opacity to 100 since for about 1 frame it is visible.
        private void Form1_Shown(object sender, EventArgs e)
        {
            HideForm(null, new EventArgs());
            Opacity = 100;
        }
    }
}
