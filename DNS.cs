﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Windows.Forms;

namespace DNS_Flusher
{
    class DNS
    {
        // Flushes the DNS. It runs a command prompt with the command: "ipconfig /flushdns" which flushes the DNS. The prompt opens with no window.
        public static string FlushDNS()
        {
            try
            {
                Console.WriteLine("Flushing DNS...");
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = "cmd.exe";
                startInfo.Arguments = "ipconfig /flushdns";
                process.StartInfo = startInfo;
                process.Start();
                Console.WriteLine("DNS Flushed.");
                Form1.Time = 18000000;
                return "DNS Flushed.";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        // Displays the DNS addresses to the user in a textbox.
        public static string DisplayDnsAddresses(TextBox dnsServersText)
        {
            try
            {
                dnsServersText.Text = "";
                StringBuilder sb = new StringBuilder();
                NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
                foreach (NetworkInterface adapter in adapters)
                {

                    IPInterfaceProperties adapterProperties = adapter.GetIPProperties();
                    IPAddressCollection dnsServers = adapterProperties.DnsAddresses;
                    if (dnsServers.Count > 0)
                    {
                        sb.AppendLine(adapter.Description);
                        foreach (IPAddress dns in dnsServers)
                        {
                            sb.AppendLine(string.Format("  DNS Servers ........................... : {0}", dns.ToString()));
                        }
                    }
                }

                dnsServersText.Text = sb.ToString();
                return "DNS addresses are now displayed.";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}
